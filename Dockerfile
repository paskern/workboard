FROM node:16.14-alpine as build

WORKDIR /app
COPY . .
RUN yarn install --immutable --immutable-cache --check-cache
RUN yarn run build

FROM nginx:stable-alpine AS deploy

WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=build /app/build .
RUN mv src/app.html index.html
RUN rm -r src
CMD ["nginx", "-g", "daemon off;"]