export default class Utils {

    static getLocalNow() {
        return new Date();
    }

    static getExceededValue(value: number, limit: number): number {
        if(value > limit) {
            return value - limit;
        }
        return 0;
    }

    static fromDateToTime(date: Date | null): string {
        if(date) {
            return `${Utils.withLeadingZero(date.getUTCHours())}:${Utils.withLeadingZero(date.getUTCMinutes())}`;
        }
        return "";
    }

    static fromLocalDateToTime(date: Date): string {
        if(date) {
            return `${Utils.withLeadingZero(date.getHours())}:${Utils.withLeadingZero(date.getMinutes())}`;
        }
        return "";
    }

    static fromTimeToDate(time: string | null): Date | null {
        const fromTheBeginging = new Date(0);
        if(time) {
            fromTheBeginging.setUTCHours(Utils.getHoursFromTime(time));
            fromTheBeginging.setUTCMinutes(Utils.getMinutesFromTime(time));
            return fromTheBeginging;
        }
        return null;
    }

    static getHoursFromTime(time: string): number {
        if(time) {
            return Number(time.split(":")[0]);
        }
        return 0;
    }

    static getMinutesFromTime(time: string): number {
        if(time) {
            return Number(time.split(":")[1]);
        }
        return 0;
    }

    static withLeadingZero(a: number): string {
        if(a < 10) {
            return `0${a}`
        }
        return `${a}`;
    }

    static chunkArray(array: Array<unknown>, chunkSize = 2): Array<any> {
        const retArray = [];
        for (let i = 0; i < array.length; i += chunkSize) {
            retArray.push(array.slice(i, i + chunkSize));
        }
        return retArray;
    }

    static returnPositiveOr0(a: number) {
        if(a < 0) {
            return 0;
        }
        return Math.abs(a);
    }

    static pairToDate(pair: WB.TimePair): WB.DatePair {
        return {from: Utils.fromTimeToDate(pair.from || ""), to: Utils.fromTimeToDate(pair.to || "")};
    }

    static getDateFromBeginning(): Date {
        const now: Date = new Date();
        return new Date(now.getHours() * 60 * 60 * 1000 + now.getMinutes() * 60 * 1000);
    }

    static getQualifiedValue(value: Record<string, unknown>, propChain: string): unknown {
        const chain: string[] = propChain.split(".");
        if(chain.length > 1) {
            const firstValue = chain.shift();
            if(firstValue) {
                const tempValue: any = value[firstValue];
                return Utils.getQualifiedValue(tempValue, chain.join("."));
            }
        } else if(chain.length === 1) {
            return value[chain[0]];
        }
        return undefined;
    }

    static setQualifiedValue(value: Record<string, any>, propChain: string, valueToSet: unknown): void {
        const props: string[] = propChain.split(".");
        if(props.length === 0) {
            console.warn("tried to set value with empty qualified propChain!");
            return;
        } else if(props.length === 1) {
            value[props[0]] = valueToSet;
        } else {
            const accessorKey: string | undefined = props.shift();
            if(accessorKey) {
                if(value[accessorKey]) {
                    value[accessorKey] = {};
                }
                Utils.setQualifiedValue(value[accessorKey], props.join("."), valueToSet);
            }
        }
    }
}