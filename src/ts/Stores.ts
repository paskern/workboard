import { writable, type Writable } from "svelte/store";
import LocalStorageManager from "./LocalStorageManager";
import {INITIAL_SETTINGS_CONFIG} from '$ts/Constants'
import Utils from "./Utils";

let initialSettings: WB.Settings | null = LocalStorageManager.getSettings();

if(!initialSettings) {
    initialSettings = INITIAL_SETTINGS_CONFIG;
}
console.log("localstorage", initialSettings)

const selectedToolStore: Writable<WB.nav> = writable<WB.nav>(initialSettings.nav);
const timecalcStore: Writable<WB.timecalc> = writable<WB.timecalc>(initialSettings.timecalc);
const toolTodoStore: Writable<WB.todoSettings> = writable<WB.todoSettings>(initialSettings.tools.todo);

let syncingBack = false;


const settings: WB.Settings = {
    nav: {...initialSettings.nav},
    timecalc: {...initialSettings.timecalc},
    tools: {...initialSettings.tools},
}

function defaultUpdate<T>(newValue: T, prop: string) {
    Utils.setQualifiedValue(settings, prop, {...newValue});
    if(!syncingBack) {
        LocalStorageManager.saveSettings(settings);
    }
}

selectedToolStore.subscribe(newValue => defaultUpdate<WB.nav>(newValue, "nav"));

timecalcStore.subscribe(newValue => defaultUpdate<WB.timecalc>(newValue, "timecalc"));

toolTodoStore.subscribe(newValue => defaultUpdate<WB.todoSettings>(newValue, "tools.todo"));

export function syncSettings(settings: WB.Settings) {
    syncingBack = true;
    selectedToolStore.set(settings.nav);
    timecalcStore.set(settings.timecalc);
    toolTodoStore.set(settings.tools.todo);
    syncingBack = false;
}

export {selectedToolStore, timecalcStore, toolTodoStore};