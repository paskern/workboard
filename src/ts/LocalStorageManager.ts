import {INITIAL_SETTINGS_CONFIG, OPTIONS} from '$ts/Constants';
import { writable, type Writable } from 'svelte/store';
import { syncSettings } from './Stores';
import {browser} from '$app/environment';

export default class LocalStorageManager {

    static settings: Writable<WB.Settings> = writable(LocalStorageManager.getSettings() || INITIAL_SETTINGS_CONFIG);

    static getSettings(): WB.Settings | null {
        if (!browser) return null;
        const settings = JSON.parse(localStorage.getItem("settings") || "null");
        LocalStorageManager.settings?.set(settings)
        return settings;
    }

    static saveSettings(settings: WB.Settings): void {
        if (!browser) return;
        const initialSettingsValue = JSON.stringify(settings);
        settings = this.ensureValidity(settings);
        console.log("saving in store...", settings, typeof settings);
        LocalStorageManager.settings.set(settings);
        localStorage.setItem("settings", JSON.stringify(settings));
        if(JSON.stringify(settings) !== initialSettingsValue) {
            syncSettings(settings);
        }
    }

    private static ensureValidity(settings: WB.Settings): WB.Settings {
        //TODO: simplify / build a construct
        const lastSelToolName = settings.nav.lastSelectedToolName;
        if(lastSelToolName && !OPTIONS.find(e => e.name === lastSelToolName)) {
            settings.nav.lastSelectedToolIndex = -1;
            settings.nav.lastSelectedToolName = null;
        }

        if(!settings.timecalc.times) {
            settings.timecalc.times = INITIAL_SETTINGS_CONFIG.timecalc.times;
        }

        if(settings.timecalc.live === undefined) {
            settings.timecalc.live = false;
        }

        if(settings.timecalc.just6 === undefined) {
            settings.timecalc.just6 = false;
        }

        if(!settings.tools) {
            settings.tools = {
                todo: {list: []}
            };
        }

        if(!settings.tools.todo) {
            settings.tools.todo = {list: []}
        }

        if(!settings.tools.todo.list) {
            settings.tools.todo.list = [];
        }

        return settings;
    }
}