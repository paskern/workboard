export class NotificationControl {

    static async requestNotification(): Promise<boolean> {
        if(Notification.permission === "granted") {
            return true;
        }
        return (await Notification.requestPermission()) === "granted";
    }

    static async sendNotification(title: string): Promise<void> {
        if(Notification.permission === "granted") {
            new Notification(title, {});
        } else {
            this.requestNotification().then((allowed) => {
                if(allowed) {
                    new Notification(title, {});
                }
            });
        }
    }
}