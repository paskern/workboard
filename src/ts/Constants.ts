import Utils from '$ts/Utils';
import Time from 'carbon-icons-svelte/lib/Time.svelte';
import Alarm  from 'carbon-icons-svelte/lib/Alarm.svelte';
import Cafe from 'carbon-icons-svelte/lib/Cafe.svelte';
import AsleepFilled from 'carbon-icons-svelte/lib/AsleepFilled.svelte';
import Settings from 'carbon-icons-svelte/lib/Settings.svelte';
import Task from 'carbon-icons-svelte/lib/Task.svelte';
import Todo from '$lib/tools/Todo.svelte';
import SettingsView from '$lib/tools/SettingsView.svelte';

export const OPTIONS: Array<WB.OPTION> = [
    {component: Todo, icon: Task, color: '#ff6e59', name: "todo"},
    //{component: null, icon: Time, color: 'orange', name: "routine"},
    //{component: null, icon: Cafe, color: 'rgb(252, 232, 55)', name: "break"},
    //{component: null, icon: AsleepFilled, color: 'rgb(55, 233, 55)', name: "mood"},
    //{component: null, icon: Time, color: '#972cee', name: "timetrack"},
    {component: SettingsView, icon: Settings, color: '#ddd', name: "settings"},

];

export const MINIMUM_WORKTOME_PER_DAY: Date = Utils.fromTimeToDate("7:30");
export const SHORT_WORK_TIME: Date = Utils.fromTimeToDate("6:00");
export const MINIMUM_BREAK_TIME_PER_DAY: Date = Utils.fromTimeToDate("0:50");
export const MINIMUM_WORK_TIME_INVEST_PER_DAY: Date = new Date(MINIMUM_WORKTOME_PER_DAY.getTime() + MINIMUM_BREAK_TIME_PER_DAY.getTime());

export const INITIAL_SETTINGS_CONFIG: WB.Settings = {
    nav: {
        lastSelectedToolIndex: -1,
		lastSelectedToolName: null
    },
    timecalc: {
        times: [{from: null, to: null}],
        live: false,
        just6: false,
        doRainbowAnimation: true
    },
    tools: {
        todo: {
            list: []
        }
    }
}