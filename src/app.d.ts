// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces

import type { CarbonIcon } from "carbon-icons-svelte"

// and what to do when importing types

declare namespace App {
	// interface Locals {}
	// interface PageData {}
	// interface Platform {}
}
declare global {
	declare namespace WB {
		interface OPTION {
			component: SvelteComponent,
			icon: typeof CarbonIcon,
			color: string,
			name: string
		}

		interface nav {
			lastSelectedToolIndex: number,
			lastSelectedToolName: string | null
		}

		interface TimePair {
			from: string | null
			to: string | null
		}

		interface DatePair {
			from: Date | null
			to: Date | null
		}

		interface timecalc {
			times: Array<TimePair>,
			live: boolean,
			just6: boolean,
			doRainbowAnimation: boolean
		}

		interface Todo {
			name: string,
			done: boolean
		}

		interface todoSettings {
			list: Array<WB.Todo>
		}

		interface toolSettings {
			todo: todoSettings
		}

		interface Settings {
			nav,
			timecalc,
			tools: toolSettings
		}

		interface LocalStorage {
			Settings
		}
	}
}