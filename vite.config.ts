import { sveltekit } from '@sveltejs/kit/vite';
import type { UserConfig } from 'vite';
import basicSsl from '@vitejs/plugin-basic-ssl'

import path from 'path';

const config: UserConfig = {
	resolve: {
		alias:{
			'$ts' : path.resolve(__dirname, './src/ts'),
		},
	},
	server: {
		https: true
	},
	plugins: [sveltekit(), basicSsl()]
};

export default config;
